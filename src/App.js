import './App.css';
import { useState } from 'react';

function App() {
  const character = 50;
  let [inputValue, setInputValue] = useState('');
  let [result, setResult] = useState();

  const handleChange = (event) => {
    setInputValue(event.target.value);
  }

  const handleSubmit = () => {
    if (inputValue) {
      setResult(splitIntoLines(inputValue, character));
    }
  }

  function splitIntoLines(input, len) {
    let i, lineSoFar = "", output = [], temp;
    var words = input.split(' ');
    for (i = 0; i < words.length;) {
      temp = addWordOntoLine(lineSoFar, words[i]);
      if (temp.length > len) {
        if (lineSoFar.length === 0) {
          lineSoFar = temp;
          i++;
        }
        output.push(lineSoFar);
        lineSoFar = "";
      } else {
        lineSoFar = temp;
        i++;
      }
    }
    if (lineSoFar.length > 0)
      output.push(lineSoFar);

    return (output);
  }

  function addWordOntoLine(line, word) {
    if (line.length !== 0)
      line += " ";

    return (line += word);
  }

  return (
    <div className="App">
      <input type="text" value={inputValue} onChange={handleChange} className={"input-box"} placeholder={"Please enter paragraph"} />
      <button type="button" onClick={() => handleSubmit()} className="button">Submit</button>

      {result && result.length > 0 &&
        <div className={"table"}>
          {
            result.map((data, index) => {
              return (
                <div key={index} className="table-row">
                  <span>{index + 1} / {result.length} {data} - {data.length}</span>
                </div>
              )
            })
          }
        </div>
      }
    </div >
  );
}

export default App;
